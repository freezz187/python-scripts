"""
Simple CDN checker tool.
Copyright 2020, Taras Lisovych <taras.lisovych@onapp.com>
"""

import os.path
import sys
import subprocess
from xmlrpclib import ServerProxy, _Method, Fault
import socket

API_URL = "https://api.onappcdn.com/core/xmlrpc"
username = ""
password = ""


class OnAppRPC(ServerProxy):

    def __init__(self, url, user, passwd):
        ServerProxy.__init__(self, url, allow_none=True)
        self.user = user
        self.passwd = passwd

    def __getattr__(self, name):
        return OnAppRPCMethod(self._ServerProxy__request,
                              name,
                              self.user,
                              self.passwd)


class OnAppRPCMethod(_Method):

    def __init__(self, send, name, user, passwd):
        _Method.__init__(self, send, name)
        self.user = user
        self.passwd = passwd

    def __getattr__(self, name):
        return OnAppRPCMethod(self._Method__send,
                              "%s.%s" % (self._Method__name, name),
                              self.user,
                              self.passwd)

    def __call__(self, *args, **kwargs):
        if "su" in kwargs:
            username = "%s?su=%s" % (self.user, kwargs["su"])
        else:
            username = self.user
        return self._Method__send(self._Method__name,
                                  (username, self.passwd) + args)


def main(args):
    os.system('clear')
    if not args:
        print("No arguments specified. Please, review docs.\n")
        print_usage()

    if args[0] in ('--check_edges_by_resource', '-er') and len(args) >= 2:
        if args[1].isdigit():
            login()
            if len(args) >= 3:
                check_edges_by_resource(args[1], args[2])
            else:
                check_edges_by_resource(args[1])

        else:
            print("ID is not integer.")

    elif args[0] in ('--check_edges_by_operator', '-eo') and len(args) >= 2:
        if args[1].isdigit():
            login()
            check_edges_by_operator(args[1])
        else:
            print("ID is not integer.")

    elif args[0] in ('--check_edges_by_ip', '-ei') and len(args) >= 2:
        login()
        if len(args) >= 3:
            check_edges_by_ip(args[1], args[2])
        else:
            check_edges_by_ip(args[1])

    elif args[0] in ('--edge_details', '-e') and len(args) >= 2:
        login()
        get_edge_details(args[1], args[2])

    elif args[0] in ('--help', '-h'):
        print_usage()

    else:
        print("\nCommand not found or not correct. Please read the manual \"--help, \'-h\".\n")
        print_usage()


def login():
    try:
        global rpc
        rpc = OnAppRPC(API_URL, username, password)
        rpc.auth.authenticate()
        print("Successfully logged in.")
    except Fault as e:
        print("Error verifying login:\n%s", e)
        sys.exit(0)


def check_edges_by_operator(operator_id, *param):
    if len(param) > 0:
        if param[0] == "--list":
            output = rpc.edges.get({'operator': [operator_id]})
            edges_count = output['totalCount']
            print("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
            print("List of EDGEs:")
            for i in range(edges_count):
                print(output['results'][i]['ip'] + " "),
            print("\n")
        else:
            print("\nUnknown option %s \n\n" % param[0])
            sys.exit(0)
    else:
        command = raw_input("What are looking for (command)? ")
        print("")
        output = rpc.edges.get({'operator': [operator_id]})
        edges_count = output['totalCount']
        for i in range(edges_count):
            ip = output['results'][i]['ip']
            print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
            print("+++ Checking the EDGE with IP %s" % ip)
            ssh_and_exec(ip, command)
            print("")
    sys.exit(0)


def check_edges_by_ip(edge_ip, *param):
    if validate_ip(edge_ip):
        operator = get_operator_by_edge_ip(edge_ip)
        if len(param) > 0:
            check_edges_by_operator(operator, param[0])
        else:
            check_edges_by_operator(operator)


def check_edges_by_resource(resource_id, *param):
    if len(param) == 0:
        command = raw_input("What are looking for (command)? ")
        print("")
    resource_id = int(resource_id)
    output = rpc.resources.get({'id': [resource_id]})
    for i in range(len(output['results'][0]['edgeGroups'])):
        print("\nEdge group \"" + output['results'][0]['edgeGroups'][i]['name'] + "\"")
        print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        if len(param) > 0:
            if param[0] == "--list":
                print("List of EDGEs:")
        for j in range(len(output['results'][0]['edgeGroups'][i]['locations'])):
            location_id = output['results'][0]['edgeGroups'][i]['locations'][j]['id']
            location = output['results'][0]['edgeGroups'][i]['locations'][j]['city']
            for edge_ip in get_edges_by_location(location_id):
                if len(param) > 0:
                    if param[0] == "--list":
                        print(edge_ip + " "),
                    else:
                        print("\nUnknown option %s \n\n" % param[0])
                        sys.exit(0)
                else:
                    print("+++ Checking the EDGE with IP %s (%s)" % (edge_ip, location))
                    ssh_and_exec(edge_ip, command)
                    print("")
        print("\n")


def get_edge_details(option, edge):
    if option == "id":
        if edge.isdigit():
            output = rpc.edges.get({'id': edge})
        else:
            print("\nProvided ID is not valid.\n")
            sys.exit(0)
    elif option == "ip":
        if validate_ip(edge):
            edge_id = get_edgeId_by_ip(edge)
            output = rpc.edges.get({'id': edge_id})
        #else:
        #    print("\nProvided IP address is not valid.\n")
        #    sys.exit(0)
    else:
        print("\nPlease, specify correct option:")
        print("\"id\" for EDGE\'s id")
        print("\"ip\" from EDGE\'s IP address")
        sys.exit(0)
    print("EDGE details:\n")
    print("\nid:\t\t\t\t" + str(output['results'][0]['id']))
    print("Name:\t\t\t\t" + str(output['results'][0]['name']))
    print("Main IP address:\t\t" + str(output['results'][0]['ip']))
    for i in range(len(output['results'][0]['ips'])):
        print("Secondary IP address:\t\t\t" + str(output['results'][0]['ips'][i]['ip']))
    print("Marketplace status:\t\t" + str(output['results'][0]['martStatus']))
    print("Network interface speed:\t" + str(output['results'][0]['speedLimit']))
    print("Status:\t\t\t\t" + str(output['results'][0]['status']))
    print("Streaming?:\t\t\t" + str(output['results'][0]['streamOn']))
    print("\nLocation id:\t\t\t" + str(output['results'][0]['location']['id']))
    print("Location country:\t\t" + str(output['results'][0]['location']['country']))
    print("Location city:\t\t\t" + str(output['results'][0]['location']['city']))
    print("Location status:\t\t" + str(output['results'][0]['location']['status']))
    print("Location price:\t\t\t" + str(output['results'][0]['location']['price']))
    print("\nOperator ID:\t\t\t" + str(output['results'][0]['operator']['id']))
    print("Operator\'s name:\t\t" + str(output['results'][0]['operator']['name']))
    print("Operator\'s company:\t\t" + str(output['results'][0]['operator']['companyName']))
    print("Operator\'s email:\t\t" + str(output['results'][0]['operator']['email']))
    print("Operator\'s status:\t\t" + str(output['results'][0]['operator']['status']))
    print("Operator\'s username:\t\t" + str(output['results'][0]['operator']['username']))
    print("Operator\'s AcceleratorOn?:\t" + str(output['results'][0]['operator']['acceleratorOn']))
    print("Operator\'s cdnOn?:\t\t" + str(output['results'][0]['operator']['cdnOn']))
    print("\nCloud id:\t\t\t" + str(output['results'][0]['cloud']['id']))
    print("Cloud license:\t\t\t" + str(output['results'][0]['cloud']['name']))
    print("\n\n")
    sys.exit(0)


def validate_ip(ip):
    try:
        socket.inet_aton(ip)
        return True
    except socket.error:
        print("\nProvided IP address is not valid.\n")
        sys.exit(0)


def ssh_and_exec(edge_ip, command):
    if is_pingable(edge_ip):
        ssh = subprocess.Popen(["sudo -u support -i ssh -o StrictHostKeyChecking=no root@%s" % edge_ip], shell=True,
                               stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        ssh.stdin.write(command + "\n")
        ssh.stdin.write("logout\n")
        ssh.stdin.close()
        start_parse = 0
        for line in ssh.stdout:
            if "permitted by applicable law" in line:
                start_parse = 1
                continue
            if start_parse == 1:
                print(line),
    else:
        print("EDGE %s unreachable.\n" % edge_ip)


def is_pingable(ip):
    #print("\nTrying to connect to ....")
    return not subprocess.call(["ping -c 3 %s" % ip], shell=True, stdout=subprocess.PIPE)


def get_edgeId_by_ip(edge_ip):
    if is_pingable(edge_ip):
        ssh = subprocess.Popen(["sudo -u support -i ssh -o StrictHostKeyChecking=no root@%s" % edge_ip], shell=True,
                           stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        ssh.stdin.write("hostname -s\n")
        ssh.stdin.write("logout\n")
        ssh.stdin.close()
        start_parse = 0
        for line in ssh.stdout:
            if "permitted by applicable law" in line:
                start_parse = 1
                continue
            if start_parse == 1:
                return line
    else:
        print("EDGE %s unreachable.\n" %edge_ip)
        sys.exit(0)

def get_operator_by_edge_ip(edge_ip):
    edge_id = get_edgeId_by_ip(edge_ip)
    return get_operator_by_edge_id(edge_id)


def get_operator_by_edge_id(edge_id):
    output = rpc.edges.get({'id': edge_id})
    return int(output['results'][0]['operator']['id'])


def get_edges_by_location(location_id):
    location_id = int(location_id)
    edge_ips = []
    output = rpc.edges.get({'location': [location_id]})
    for i in range(len(output['results'])):
        edge_ips.append(output['results'][i]['ip'])
    return edge_ips


def print_usage():
    print("Usage: python cdn_checker.py [OPTION] [PARAM]")
    print("")
    print("Options:")
    print("-h, --help\t\t\tShow help")
    print(
        "-er, --check_edges_by_resource\tExecute some commands on all EDGEs assigned for the CDN resource. Option \'--list\' is available.")
    print("-eo, --check_edges_by_operator\tExecute some commands on all EDGEs owned by the CDN operator.")
    print(
        "-ei, --check_edges_by_ip\tFind the operator of the EDGE, select all his EDGEs and execute some commands on those EDGEs. Option \'--list\' is available.")
    print("-e, --edge_details\t\tPrint EDGE details. Followed by \'ip\' for details by IP or \'id\' for details by EDGE\'s ID.")
    print("\n--list\t\t\t\tPrint list of EDGEs only (for your custom scripts)\n")
    print("")
    print("Example:")
    print("python cdn_checker.py --check_edges_by_resource 752317828 --list")
    print("python cdn_checker.py -h\n")
    sys.exit(0)


if __name__ == "__main__":
    main(sys.argv[1:])
