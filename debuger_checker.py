import os
import subprocess

host ="debug.onappcdn.com"
COMMAND="less /var/log/nginx/access.log | awk '($7 ~ /500/)' | wc -l\n"

cmd = "host "+host
exec_result = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)

ips = []

print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
print("++ Checking the host %s ..." %host)
print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
print("")

for line in exec_result.stdout:
        if "address" in line:
                data = line.split()
                ips.append(data[3])

print("IP addresses are %s" % ips)
print("")

for ip in ips:
        print("Checking the logs on %s" % ip)
        ssh = subprocess.Popen(["sudo -u support -i ssh -o StrictHostKeyChecking=no %s" % ip], shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        ssh.stdin.write(COMMAND)
        ssh.stdin.write("logout\n")
        ssh.stdin.close()
        start_parse=0
        for line in ssh.stdout:
                if "permitted by applicable law" in line:
                        start_parse=1
                        continue
                if start_parse == 1:
                        print("The amount of 500 HTTP responses in /var/log/nginx/access.log  - %s" % line)
